#!/usr/bin/env bash

server_host=$SERVER_HOST
server_user=$SERVER_USER
app_path="/home/$server_user/app"

function copy_files() {
  ssh -i private_key -o StrictHostKeyChecking=no -o StrictHostKeyChecking=no \
    "$server_user"@"$server_host" "mkdir -p $app_path"

  source_files=(
    "docker-compose.prod.yml"
    ".env"
    "react-frontend/nginx.conf"
    "react-frontend/.env"
    "nginx/nginx.conf"
  )

  for file in "${source_files[@]}"; do
    ssh -i private_key -o StrictHostKeyChecking=no -o StrictHostKeyChecking=no \
      "$server_user"@"$server_host" "mkdir -p $app_path/$(dirname "$file")"
    scp -i private_key -o StrictHostKeyChecking=no -o StrictHostKeyChecking=no \
      "$file" "$server_user"@"$server_host":"$app_path"/"$file"
  done
}

function dockerize() {
   echo "Dockerizing the app"
}

function main() {
  required_env_vars=("SERVER_HOST" "SERVER_USER")

  for env_var in "${required_env_vars[@]}"; do
    if [ -z "${!env_var}" ]; then
      echo "Error: $env_var is not set"
      exit 1
    fi
  done

  copy_files
  dockerize
}

main